+++
title = "Nueve libros sobre usabilidad y programación."
date = "2019-10-15"
draft = false
thumbnail = ""
description = "Libros para leer"
author = "koffer"
author_uri = "/directorio/koffer"
markup = "mmark"
+++

Estos son algunos los libros que recomiendan los integrantes de Xalapacode. Es interesante empezar a leer algunos libros basándose en recomendaciones de otros.

![Clean Code](/img/blog/51d1qVhmAmL.jpg)

**Clean Code de Robert C. Martin** y Software Requirements de Karl Wiegers - en clean code se describen los principios de las buenas practicas de realizar un código limpio y me parece que es primordial para todos aquellos que queremos mejorar en el prceso de desarrollo y por parte de Software Requirements puede utilizarse como una guía practica para la etapa de la elicitación de requerimientos la cual es la base para un exitoso desarrollo de software #libros  Recomendado por Victor Hernandez


![Usability Testing Essentials](/img/blog/51QouQqpNvL.jpg)

**Usability Testing Essentials** de Carol M. Barnum, explica todo acerca hacer pruebas, desde qué hacer en las pruebas, cómo hablar con los participantes, qué herramientas utilizar, cómo analizar los resultados, cómo presentar el reporte final. Todo viene ilustrado con ejemplos (incluye formatos de herramientas de evaluación)

Recomendando por Tere Cepero

![Usability Testing Essentials](/img/blog/pooymvc.png)

**POO y MVC en PHP** Este libro hace años me ayudo mucho aprender php, mas que nada a ser mas organizado con el lenguaje y darme una idea de como se implementa uno de los patrones de diseños mas utilizados en esa epoca (#MVC que ahorita el que ocupan frameworks como laravel es una variante del mismo) se que la mayoria le hace fuchi a #PHP pero creo que no esta de mas 😆#libros #español  Recomendando por Mario contreras

![Usability Testing Essentials](/img/blog/everyday.png)

**The Design of everyday things**, Don Norman. No es un libro de IT, sino de como es que deben de diseñarse las cosas con las que interactuamos en el dia a dia. Todos los principios que ahi se explican se pueden trasladar al diseño de interfaces o incluso al mismo codigo. Se puede consultar aqui: https://www.nixdell.com/classes/HCI-and-Design-Spring-2017/The-Design-of-Everyday-Things-Revised-and-Expanded-Edition.pdf

Recomendando por Mauricio Tellez


![Usability Testing Essentials](/img/blog/book-software.png)

**The Nature of Software Development** de Ron Jeffries; no me cansare de recomendar este libro (tercera vez que lo recomiendo aqui); este libro tiene un enfoque super sencillo y atractivo sobre como entregar valor a traves del software de a poquito, quizas sea chocante para managers y clientes que quieren que les construyas el empire state building en tres meses y tambien por eso esta vez la recomendacion va para ellos primero y lo pondre en negritas para recalcarlo

la proxima vez que alguien les diga: “tengo esta idea y va a resolver todos estos problemas de entrada y lo vamos a hacer en una cantidad ridiculamente pequeña de tiempo” mandenle a leer ese libro 


Recomendando por Mauricio Tellez

![Usability Testing Essentials](/img/blog/sistemas-operativos.png)

**Sistemas operativos, diseño e implementación** de Andrew s. tanenbaum. No solo explica de manera muy clara los componentes que forman a un sistema operativo, sino que usa el codigo de minix para ver una implementacion real de los conceptos. Por ejemplo, nosotros damos por hecho que la memoria esta ahi para almacenar los procesos en ejecucion, pero nos hemos preguntado que pasa cuando se llena la ram y tiene que salir un proceso para que entre otro? Esta y otras muchas preguntas mas son contestadas en el libro.

Recomendando por Jail

