+++
title = "Meetup 13 de Septiembre 2019"
publishdate = "2019-08-27"
date = "2019-09-13"
event_place = "Aula Clavijero"
description = "El evento de septiembre, nuestro mes patrio"
thumbnail = "/img/eventos/meetup-2019-09-13.jpg"
author = "categulario"
draft = false
turnout = 30
+++

## Las platicas

1. **Pafu postmortem: cocinando a distancia**, por Harry Jackson
2. **Solo hay dos cosas seguras: la muerte y hacienda**, por César Encarnación Mendoza
3. **Web Reactiva**, por Isidro Hernández Gregorio
4. **¿Afore, Bolsa, Oro, Divisas?**, por Lina Beauregard

## Sobre la sede

Es una sala de proyección equipada con 105 butacas, cañón proyector de video, equipo de sonido, pantalla, clima y servicios sanitarios. Se encuentra en el sótano del edificio que una vez albergó a la Rectoría y a la Editorial de la Universidad Veracruzana, en el número 55 de la calle Juárez en el centro histórico de Xalapa.

[Mapa](https://www.google.com/maps/place/Aula+Clavijero/@19.528957,-96.9250825,17z/data=!3m1!4b1!4m5!3m4!1s0x85db2dffa58d2bcf:0xdde28f4df6189350!8m2!3d19.528957!4d-96.9228938)

## Sobre el evento

Los meetups de xalapacode son encuentros en los que se realizan una serie de desconferencias de 15 minutos de duración por miembros de la comunidad como tu sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
